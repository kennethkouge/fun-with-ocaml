let max_of_list a = 
	let first_of_list a = match a with
		| [] -> failwith "empty list"
		| e :: _ -> e in
	let max_acc acc x = if x > acc then x else acc in
	List.fold_left
		max_acc
		(first_of_list a)
		a;;

print_int (max_of_list [1; 2; 8; 4]);;